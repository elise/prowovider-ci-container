FROM docker.io/rust:latest

ENV MOLD_VERSION="1.4.2"

RUN apt update && apt install clang -y
RUN curl -L https://github.com/rui314/mold/releases/download/v$MOLD_VERSION/mold-$MOLD_VERSION-x86_64-linux.tar.gz | tar -z --extract --directory=/bin/ --strip=2 --file=- mold-$MOLD_VERSION-x86_64-linux/bin/mold
RUN rustup component add rustfmt